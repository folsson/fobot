#!/usr/bin/env python3
# Program for downloading HTML that loads dynamically by scrolling.
# Usage:
# From cli: python3 run.py
# inputonex: http://www.allabolag.se/branch/bank-finans-forsakring/14/_/page/1
# inputwoex: 3
# inputhreex: 9
from general import *
from botcontroller import BotController

def main():
    print('foBot v0.2')
    start_page = input('URL > ')
    pages_to_scroll = input('Number of scrolls > ')
    end_page = input('End page > ')
    # Starts a bot.  
    bot = BotController(start_page, int(pages_to_scroll), int(end_page))
    bot.run()

if __name__ == '__main__':
    main()
