#!/usr/bin/env python3
# Class for controlling the bots
from general import *
from scraper import Scraper

class BotController:
    # Initialize controller with a scraper object
    # Formatting for allabolag atm
    def __init__(self, start_page, pages_to_scroll, end_page):
        print(time_now() + 'Bot initializing.')
        self._start_page = start_page
        self._pages_to_scroll = pages_to_scroll
        self._end_page = end_page
        create_dir('data/logs')
        create_dir('data/htmldumps')
        self._dump_path = os.getcwd() + '/data/htmldumps/'
 
        #alla bolag setup
        self._page_state = int(self._start_page.split('/page/', 1)[1])

    def scrape_pages(self):
        url_counter = self._page_state
        for count in range(0, self._end_page, self._pages_to_scroll):
            dump_name = '{0}topage{1}.html'.format(self._start_page
                    .replace('/', '.').replace(':', '.'),
                    int(count + self._pages_to_scroll)
                    ).replace('.page.1', '.page.{0}'.format(url_counter))
            scrape = Scraper(self._start_page.replace('/page/1', '/page/{0}'
                .format(url_counter)), self._pages_to_scroll)
            print(time_now() + 'Scraping page: {0} to {1}.'
                    .format(url_counter, int(url_counter - 1) 
                        + int(self._pages_to_scroll)))
            datadump = scrape.run()
            print(time_now() + 'Saving data.')
            write_file(self._dump_path + dump_name, datadump)
            url_counter += self._pages_to_scroll

    def run(self):
        self.scrape_pages()
        self.terminate()

    def terminate(self):
        print(time_now() + 'Bot terminated. Bye!')
