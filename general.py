#!/usr/bin/env python3
# Module with general global functions
import sys, os, time, datetime, random

# Create file and write to it.
def write_file(filename, data):
    with open(filename, mode='w', encoding='utf-8') as f:
        for line in data:
            f.write(line)
        f.write('\n')
        f.close()

# Append to file.
def append_file(filename, data):
    with open(filename, 'a') as f:
        f.write(data + '\n')
        f.close()

# Create directory with foBot as rootfolder.
def create_dir(dir_name):
    path = os.getcwd() + '/{0}/'.format(dir_name)
    if not os.path.exists(path):
        os.makedirs(path)
        print(time_now() + 'Created dir: {0}'.format(path))

# Returns a date and time-stamp.
def time_now():
    return str('{0:%H:%M:%S} '.format(datetime.datetime.now()))

# Returns random int between i_one and i_two
def random_int(i_one, i_two):
    return random.randrange(i_one, i_two)
