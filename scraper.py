#!/usr/bin/env python3
# Class for working with selenium and phantomjs.
from general import *
from selenium import webdriver

class Scraper:
    # Initialize the page scraper and load start page.
    def __init__(self, start_page, pages_to_scroll):
        print(time_now() + 'Scraper initializing.')
        self._driver = webdriver.PhantomJS()
        print(time_now() + 'Scraper loading: {0}.'.format(start_page))
        self._load_page = self._driver.get(start_page)
        print(time_now() + 'Loaded.')
        self._start_page = start_page
        self._pages_to_scroll = pages_to_scroll 

        # alla bolag setup
        self._page_state = int(self._start_page.split('/page/', 1)[1])
       
    def get_html(self):
        html_element = self._driver.find_element_by_tag_name('html')
        html_data = html_element.get_attribute('innerHTML')
        return html_data

    # Randomized scroll 
    def scroll(self):
        page_state = self._page_state
        print(time_now() + 'Starting scroll.')
        for count in range(self._pages_to_scroll):
            random = random_int(5, 18)
            print('\r' + time_now() + 'Sleeping: ' + str(random) + 'sec. ' 
                    + 'Scrolling: {0}/{1}.                         '
                    .format(count + 1, self._pages_to_scroll), 
                    end='', flush=True)
            time.sleep(random)
            self._driver.execute_script(
                    'window.scrollTo(0, document.body.scrollHeight);')
            self.create_logs(page_state)
            page_state += 1
        print('\n' + time_now() + 'Scroll complete.')

    # allabolag formated logs
    def create_logs(self, last_crawled):
        # Create logs. Currently hardcoded
        file_name = self._start_page.replace('/page/' + str(self._page_state), 
                '/page/' + str(last_crawled))
        log_path = os.getcwd() + '/data/logs/lastcrawled.log'
        if not os.path.exists(log_path):
            write_file(log_path, time_now() + file_name)
            print('\r' + time_now() + 'Created crawl-log.                  ',
                    end='', flush=True)
        else:
            append_file(log_path, time_now() + file_name)
            print('\r' + time_now() + 'Updated crawl-log.                  ', 
                    end='', flush=True)

    def run(self):
        self.scroll()
        payload = self.get_html()
        self.terminate()
        return payload

    def terminate(self):
        self._driver.quit()
        print(time_now() + 'Scraper terminated.')
